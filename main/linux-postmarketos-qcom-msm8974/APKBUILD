# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.9.13
pkgrel=1
_commit="ad989a3e7324563b3a85e521e052fec2c6b56752"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	"
source="https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="935b1949e659ba83a54430346c7e4a1c32cc51910e2300b89ed54e2d3da890b281cfea51eb12af633c1f6c94c599e95842b81c6db04fbb5b2dbded46d8d0180a  linux-postmarketos-ad989a3e7324563b3a85e521e052fec2c6b56752.tar.gz
7a39434a698cd990416daa48ef642b8ed62a0814d79fe9796da34f0cfe61d22d4a8b7b6f74ad63be93c5a6918f5c6b435d7ae37bbdb389635dc7c7a0b9351526  config-postmarketos-qcom-msm8974.armv7"
