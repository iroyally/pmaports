# Maintainer: Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Oliver Smith <ollieparanod@postmarketos.org>
#
# Note: cannot upstream to Alpine until:
# https://gitlab.alpinelinux.org/alpine/apk-tools/-/issues/10712
pkgname=osk-sdl
pkgver=0.63
pkgrel=0
pkgdesc="Onscreen keyboard for unlocking LUKS devices"
url="https://gitlab.com/postmarketOS/osk-sdl"
arch="all"
license="GPL-3.0-or-later"
depends="
	cryptsetup-libs
	directfb
	mesa-gl
	mesa-gles
	ttf-dejavu
	"
makedepends="
	cryptsetup-dev
	linux-headers
	meson
	scdoc
	sdl2-dev
	sdl2_ttf-dev
	"
source="https://gitlab.com/postmarketOS/osk-sdl/-/archive/$pkgver/osk-sdl-$pkgver.tar.gz"
options="!strip !check" # No tests
subpackages="$pkgname-doc"

build() {
	abuild-meson build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build

	# needed to trigger initfs rebuild:
	touch osk-sdl
	install -Dm644 osk-sdl \
		"$pkgdir"/usr/share/postmarketos-mkinitfs-triggers/osk-sdl
}

sha512sums="56c5c0ec85f340324cecf42a1b5c583c0d744ece6cf4d13986d10ff533799df05c2b44a13ccceb98230350df6b7963e0713de75cc9df7227d379845cd8964a77  osk-sdl-0.63.tar.gz"
