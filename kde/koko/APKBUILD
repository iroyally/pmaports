# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=koko
pkgver=0_git20210114
pkgrel=0
_commit="c9e9b678fe781e988276bf72f31f899ceaecbf1b"
_geonames_pkgver=2020.06.25
pkgdesc="Image gallery application for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/koko"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="LGPL-2.0-or-later AND LGPL-2.1-only AND LGPL-3.0-only AND LicenseRef-KDE-Accepted-GPL"
depends="
	kdeclarative
	kirigami2
	purpose
	qt5-qtbase-sqlite
	"
makedepends="
	exiv2-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	"
source="https://invent.kde.org/plasma-mobile/koko/-/archive/$_commit/koko-$_commit.tar.gz
	https://github.com/pmsourcedump/geonames/archive/$_geonames_pkgver/geonames-$_geonames_pkgver.tar.gz
	"
options="!check" # Broken tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	mv "$srcdir"/geonames-$_geonames_pkgver/cities1000.zip src/
	mv "$srcdir"/geonames-$_geonames_pkgver/admin1CodesASCII.txt src/
	mv "$srcdir"/geonames-$_geonames_pkgver/admin2Codes.txt src/

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="84fd88a32d7eb1373d7455d269fa579fb40adde809960f83e18255bf995bd982b7f824d9bbf7a97a383ac169b1ab28eb2764f44bb0fba0a5c0cfa1748028e98a  koko-c9e9b678fe781e988276bf72f31f899ceaecbf1b.tar.gz
07485983a5ce0f03f1e12e64c280abe01e81beaa22b2dd43bc1b0e7632298acbfb83f09ef8c01a0915481c8e918a430b97d68f1ed5d43f76506798245345bc14  geonames-2020.06.25.tar.gz"
